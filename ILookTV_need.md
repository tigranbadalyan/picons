# українські
* 33 канал

# детские
* Мультиландия
* Уникум

# HD
* Clubbing TV HD RU
* Setanta Qazaqstan HD
* Губерния 33 HD
* Футбол 3 HD
* 568 HD
* День Победы HD

# взрослые
* Exxxotica HD
* XXL
* Penthouse Gold HD
* Penthouse Quickies 1300k
* Penthouse Quickies HD

# спорт
* Телеканал Футбол

# Հայկական
* Ազատություն TV
* ՖՒԼՄԶՈՆ
* ԽԱՂԱԼԻՔ
* քոմեդի
* Արցախ
* տունտունիկ
* ՀԱՅ TV
* ԹԱՎԱ TV
* ԿԻՆՈՄԱՆ
* սինեման
* նոր ՀԱՅԱՍՏԱՆ
* ջան tv
* հայ կինո
* Shant Serial HD
* ֆիտնես
* մուզզոն
* Բազմոց tv
* Shant Premium HD AM
* Shant Kids HD

# azərbaycan
* Region TV
* ARB 24
* Gunesh
* Space TV
* Lider TV
* AMC AZ
* CBC Sport
* Ictimai TV
* CBC
* Medeniyyet
* Idman
* Xazar TV
* Azad TV

# ქართული
* 1 TV
* GDS TV
* Maestro
* Imedi TV
* TV 25
* Pirveli
* Obieqtivi TV
* Ajara TV
* Palitra news

# қазақстан
* 31 канал KZ
* НТК
* СТВ KZ
* ТАН
* 5 канал KZ
* Казахстан Караганда

# точик
* TV Varzish HD
* TV Sinamo
* Tojikistan HD
* Safina HD
* Bakhoristan HD
* Jahonnamo

# o'zbek
* Ozbekiston
* Yoshlar
* Toshkent
* UzSport
* Madeniyat va marafat
* Dunyo
* Bolajon
* Navo
* Kinoteatr
* Oilaviy
* Uzbekistan 24

# moldovenească
* Orhei TV
* PRIMUL
* ITV Moldova
* СТС Mega MD
* TVR1 HD
* MBC MD
* GOLD TV MD
* Noroc TV
* AXIAL TV

# türk
* A SPORT
* Kanal 7 HD
* 24 TV
* A HABER
* A2
* ATV TR
* DREAM TURK
* minikaGO
* NR1 TURK TV HD
* STAR TV
* TRT AVAZ
* TRT Çocuk
* TRT DIYANET
* TRT Haber
* TRT Müzik
* TRT World
* TRT 1
* TV79
* TV8 HD
* MIXBOX SINEMA 3
* MIXBOX SINEMA 4
* DMAX
* TRT 1 HD
* A HABER HD
* POWER HD
* POWERTURK HD
* NTV HD
* DMAX HD TR
* STAR TV HD
* NTV
* Minika Çocuk
* Kanal D HD
* CNN Turk HD
* Teve 2 HD
* Show TV HD
* Haberturk HD
* Fox Turkiye HD
* Beyaz TV HD
* Ulke TV HD
* Kanal S
* Bloomberg HT
* 5 HD
* 24 TV HD
* 360 HD
* TV 4 HD
* beIN SPORTS HABER
* beIN SPORTS HABER HD
* CARTOON NETWORK TR
* KANAL D
* TV 100 HD

# ישראלי
* Channel 12 IL
* Channel 13 IL
* Channel 9 IL
* Channel 11 IL
* Disney Jr IL
* TeenNick IL
* Channel 20 IL
* National Geographic IL
* Nick Jr IL
* hop IL
* Home Plus IL
* ONE HD IL
* Sport 5 HD IL
* SPORT 2 HD IL
* Sport 1 HD IL
* Hot HBO HD IL
* Good Life IL
* HOT cinema 4 IL
* HOT cinema 1 IL
* Discovery HD IL
* ZOOM IL
* ZONE HD IL
* Travel Channel IL
* Sport 5+ Live HD IL
* Turkish Dramas Plus IL
* Luli IL
* Kids IL
* Junior IL
* HOT3 HD IL
* Hop! Childhood IL
* Channel 98 IL
* Baby IL
* HOT movies 2 IL
* HOT movies 3 IL
* ONE 2 HD IL
* Channel 9 HD IL
* AMC IL
* Ego Total IL
* Food Network IL
* Health IL
* Entertainment IL
* KAN23 IL
* Makan33 IL
* Viva+ IL
* Yaldut IL
* Bombay IL
* HIDABRUT IL
* i24 IL
* VIVA IL
* Sport 3 HD IL
* Sport 4 HD IL
* Yamtihoni IL
* Sport 5 IL
* האח הגדול

# HD Orig
* DTX HD orig
* VIASAT Sport HD orig
* МАТЧ! Футбол 1 HD 50 orig
* Xsport HD orig
* Viasat Fotboll HD SK orig
* Дом кино Премиум HD orig
* Футбол 3 HD orig
